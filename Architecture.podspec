#
#  Be sure to run `pod spec lint Architecture.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

    spec.name             = 'Architecture'
    spec.version          = '0.0.1'
    spec.license          = { :type => 'BSD' }
    spec.homepage         = 'https://misterbond18@bitbucket.org/misterbond18/architecture'
    spec.authors          = { 'Ilya Bondarenko' => 'bondarenko@dnt-lab.com' }
    spec.summary          = 'Standard architecture'
    spec.source           = { :git => 'https://misterbond18@bitbucket.org/misterbond18/architecture.git', :tag => '0.0.1' }
    spec.source_files     = 'Architecture/*'
    spec.framework        = 'Architecture'
    spec.requires_arc     = true

end
