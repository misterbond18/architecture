//
//  ScenarioTests.swift
//  ArchitectureTests
//
//  Created by Ilya Bondarenko on 19.08.2019.
//  Copyright © 2019 Design and Test Lab. All rights reserved.
//

import XCTest
@testable import Architecture

class ScenarioTests: XCTestCase {
    func testStartScenario() {
        let viewController = UIViewController()
        let scenario = Scenario(rootVC: viewController)
        scenario.start()
        XCTAssert(scenario.rootVC == viewController, "The root view controllers must be equal to viewController")
        XCTAssert(scenario.childScenarios != nil, "The child scenarios array can't be nil")
        XCTAssert(scenario.childScenarios.count == 0, "The count of the child scenarios should be equal to 0")
    }
    
    func testStartAnotherScenario() {
        class FirstScenario: Scenario {
            override func start() {
                super.start()
                let secondScenario = SecondScenario(delegate: self, rootVC: rootVC)
                start(scenario: secondScenario)
            }
        }
        
        class SecondScenario: Scenario {}
        
        let viewController = UIViewController()
        let firstScenario = FirstScenario(rootVC: viewController)
        firstScenario.start()
        XCTAssert(firstScenario.rootVC == viewController, "The root view controllers must be equal to viewController")
        XCTAssert(firstScenario.childScenarios != nil, "The child scenarios array can't be nil")
        XCTAssert(firstScenario.childScenarios.count == 1, "The count of the child scenarios should be equal to 1")
        let secondScenario = (firstScenario.childScenarios.first as? SecondScenario)
        XCTAssert(secondScenario != nil, "The child scenario should be is SecondScenario")
        
        secondScenario?.delegate?.didFinish(scenario: secondScenario!)
        XCTAssert(firstScenario.childScenarios.count == 0, "The count of the child scenarios should be equal to 0")
    }
    
    func testAddParentScenarioAsChildScenario() {
        let viewController = UIViewController()
        let scenario = Scenario(rootVC: viewController)
        scenario.start(scenario: scenario)
        
        XCTAssert(scenario.childScenarios.count == 0, "The count of the child scenarios should be equal to 0")
    }
}
